package com.foreach.service;

import static com.foreach.ExecutorServiceConfig.FIXED_THREAD_POOL;
import static com.foreach.ExecutorServiceConfig.SINGLE_THREADED;
import static org.mockito.ArgumentMatchers.any;

import java.util.concurrent.ExecutorService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foreach.Config;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class LogAnalyzerServiceTest {
    @MockBean
    private ObjectMapper objectMapper;
    @MockBean
    private LogConsumer logConsumer;
    @MockBean
    private Config config;
    @MockBean
    @Qualifier(SINGLE_THREADED)
    private ExecutorService consumers;
    @MockBean
    @Qualifier(FIXED_THREAD_POOL)
    private ExecutorService producers;

    private LogAnalyzerService logAnalyzerService;

    @BeforeEach
    void init() {
        Mockito.lenient().when(config.getQueueCapacity()).thenReturn(10);
        logAnalyzerService = new LogAnalyzerService(producers, consumers, objectMapper, logConsumer, config);
    }

    @Test
    void analyzeLog() {
        //given
        String filePath = "classpath:test-log-example.txt";
        //when
        logAnalyzerService.analyzeLog(filePath);
        //then
        Mockito.verify(consumers, Mockito.times(1)).execute(any());
        Mockito.verify(producers, Mockito.times(4)).execute(any());
    }

}
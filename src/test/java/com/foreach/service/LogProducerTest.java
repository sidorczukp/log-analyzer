package com.foreach.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.foreach.Config;
import com.foreach.model.Event;
import com.foreach.model.EventData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class LogProducerTest {
    @MockBean
    private Config config;

    private BlockingQueue<Event> queue;
    private EventData eventData;
    private Set<EventData> notPersistedEvents;

    private LogProducer logProducer;

    @BeforeEach
    void init() {
        queue = new LinkedBlockingQueue<>();
        eventData = EventData.builder()
            .withId("1")
            .withHost("host")
            .withState(EventData.State.STARTED)
            .withTimestamp(Timestamp.valueOf(LocalDateTime.now()).getTime())
            .build();
        notPersistedEvents = new HashSet<>();
    }

    @DisplayName("START event is logged, FINISH event is analyzed")
    @Test
    void finalize_event() {
        //given
        EventData finishEvent = eventData.toBuilder().withState(EventData.State.FINISHED).build();
        notPersistedEvents.add(eventData);
        logProducer = new LogProducer(queue, config, notPersistedEvents, finishEvent);
        //when
        logProducer.run();
        //then
        assertThat(notPersistedEvents.size(), is(0));
        assertThat(queue.size(), is(1));
    }

    @DisplayName("Event has STARTED and was not analyzed before")
    @Test
    void new_event() {
        //given
        logProducer = new LogProducer(queue, config, notPersistedEvents, eventData);
        //when
        logProducer.run();
        //then
        assertThat(notPersistedEvents.size(), is(1));
        assertThat(queue.size(), is(0));
    }

    @DisplayName("Illegal data exception - two events with the same id and state in log")
    @Test
    void two_events_with_the_same_id_and_state() {
        notPersistedEvents.add(eventData);
        logProducer = new LogProducer(queue, config, notPersistedEvents, eventData);
        //then
        Assertions.assertThrows(IllegalStateException.class, () -> logProducer.run());
    }

    @DisplayName("Validate final event creation")
    @Test
    void valid_event_creation() {
        //given
        long duration = 10;
        EventData finishEvent = eventData.toBuilder()
            .withState(EventData.State.FINISHED)
            .withTimestamp(eventData.getTimestamp() + duration)
            .build();
        notPersistedEvents.add(eventData);

        when(config.getDurationLimit()).thenReturn(duration - 1);

        logProducer = new LogProducer(queue, config, notPersistedEvents, finishEvent);
        //when
        logProducer.run();
        //then
        Event event = queue.poll();
        assertThat(event.getId(), is(finishEvent.getId()));
        assertThat(event.getDuration(), is(duration));
        assertThat(event.getType(), is(finishEvent.getType()));
        assertThat(event.isAlert(), is(true));
    }
}
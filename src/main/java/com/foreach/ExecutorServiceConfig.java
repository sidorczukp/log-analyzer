package com.foreach;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExecutorServiceConfig {

    public static final String FIXED_THREAD_POOL = "fixedThreadPool";
    public static final String SINGLE_THREADED = "singleThreaded";

    @Bean(FIXED_THREAD_POOL)
    public ExecutorService fixedThreadPool() {
        return Executors.newFixedThreadPool(4);
    }

    @Bean(SINGLE_THREADED)
    public ExecutorService singleThreadedExecutor() {
        return Executors.newSingleThreadExecutor();
    }
}

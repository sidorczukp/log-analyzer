package com.foreach.service;


import java.util.Set;
import java.util.concurrent.BlockingQueue;

import com.foreach.Config;
import com.foreach.model.Event;
import com.foreach.model.EventData;

public class LogProducer implements Runnable {
    private final Config config;
    private final BlockingQueue<Event> queue;
    private final Set<EventData> notPersistedEvents;
    private final EventData eventData;

    LogProducer(BlockingQueue<Event> q,
                Config config,
                Set<EventData> notPersistedEvents,
                EventData eventData) {
        this.queue = q;
        this.notPersistedEvents = notPersistedEvents;
        this.eventData = eventData;
        this.config = config;
    }

    @Override
    public void run() {
        produce();
    }

    private void produce() {
        EventData founded = notPersistedEvents.stream()
            .filter(event -> event.getId().equals(eventData.getId()))
            .findFirst()
            .orElse(null);

        if (founded == null) {
            notPersistedEvents.add(eventData);
        } else {
            if (founded.getState().equals(eventData.getState())) {
                throw new IllegalStateException(
                    String.format("There is duplicated event with id %s the same state %s", eventData.getId(),
                        eventData.getState().name()));
            }
            notPersistedEvents.remove(founded);
            queue.offer(createEvent(founded, calculateDuration(founded, eventData)));
        }
    }

    private Event createEvent(EventData eventData, long duration) {
        Event event = new Event();
        event.setDuration(duration);
        event.setHost(eventData.getHost());
        event.setId(eventData.getId());
        if (duration > config.getDurationLimit()) {
            event.setAlert(true);
        }
        return event;
    }

    private long calculateDuration(EventData first, EventData second) {
        long duration;
        if (first.getState().equals(EventData.State.FINISHED)) {
            duration = first.getTimestamp() - second.getTimestamp();
        } else {
            duration = second.getTimestamp() - first.getTimestamp();
        }
        return duration;
    }

}

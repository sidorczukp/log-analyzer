package com.foreach.service;


import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import com.foreach.Config;
import com.foreach.model.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class LogConsumer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogConsumer.class);
    private static final int THREAD_THRESHOLD = 10;
    private static final int NOT_FULL_BATCH_THRESHOLD = 5;
    private final Config config;
    private final EntityManager entityManager;
    private AtomicInteger counter = new AtomicInteger();
    private BlockingQueue<Event> queue;

    LogConsumer(Config config, EntityManager entityManager) {
        this.config = config;
        this.entityManager = entityManager;
    }

    public void setQueue(BlockingQueue<Event> queue) {
        this.queue = queue;
    }

    @Override
    @Transactional
    public void run() {
        consume();
    }

    private void consume() {
        try {
            while (!Thread.currentThread().isInterrupted() && counter.get() < THREAD_THRESHOLD) {
                if (queue.size() < config.getDbBatchSize() || queue.size() == 0) {
                    counter.getAndIncrement();
                }

                if (queue.size() != 0 && counter.get() >= NOT_FULL_BATCH_THRESHOLD) {
                    Set<Event> eventsToPersist = new HashSet<>();
                    queue.drainTo(eventsToPersist, config.getDbBatchSize());
                    eventsToPersist.forEach(entityManager::persist);
                    entityManager.flush();
                    counter.getAndSet(0);
                }
                //Give a time to fill a queue
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            LOGGER.debug("Consumer thread has been interrupted!");
        }
    }
}

package com.foreach.service;


import static com.foreach.ExecutorServiceConfig.FIXED_THREAD_POOL;
import static com.foreach.ExecutorServiceConfig.SINGLE_THREADED;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foreach.Config;
import com.foreach.model.Event;
import com.foreach.model.EventData;
import com.foreach.util.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class LogAnalyzerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogAnalyzerService.class);
    private final ObjectMapper objectMapper;
    private final LogConsumer logConsumer;
    private final Config config;
    private final BlockingQueue<Event> queue;
    private final Set<EventData> notPersistedEvents = ConcurrentHashMap.newKeySet();
    private final ExecutorService producers;
    private final ExecutorService customer;

    public LogAnalyzerService(@Qualifier(FIXED_THREAD_POOL) ExecutorService producers,
                              @Qualifier(SINGLE_THREADED) ExecutorService customer,
                              ObjectMapper objectMapper,
                              LogConsumer logConsumer,
                              Config config) {
        this.producers = producers;
        this.customer = customer;
        this.objectMapper = objectMapper;
        this.logConsumer = logConsumer;
        this.config = config;
        this.queue = new LinkedBlockingQueue<>(config.getQueueCapacity());
    }

    public void analyzeLog(String filePath) {
        if (filePath == null) {
            filePath = config.getLogFilePath();
        }
        try {
            logConsumer.setQueue(queue);
            customer.execute(logConsumer);
            FileUtil.scanFile(filePath, line -> {
                EventData eventData = parseLog(line);
                LogProducer logProducer = new LogProducer(queue, config, notPersistedEvents, eventData);
                producers.execute(logProducer);
            });
        } finally {
            producers.shutdown();
            customer.shutdown();
        }
    }

    private EventData parseLog(String log) {
        try {
            return objectMapper.readValue(log, EventData.class);
        } catch (IOException e) {
            LOGGER.warn("Log line: {} was not formatted properly and was not parsed", log);
            return null;
        }
    }
}

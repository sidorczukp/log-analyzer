package com.foreach.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

public class FileUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    public static void scanFile(String filePath, Consumer<String> consumer) {
        try {
            File f = ResourceUtils.getFile(filePath);
            FileInputStream inputStream = new FileInputStream(f);
            Scanner sc = new Scanner(inputStream, "UTF-8");
            if (!sc.hasNextLine()) {
                LOGGER.info("File {} is empty", filePath);
            }
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                consumer.accept(line);
            }

        } catch (FileNotFoundException e) {
            LOGGER.error("No file with specified path {} was found", filePath);
        }
    }
}

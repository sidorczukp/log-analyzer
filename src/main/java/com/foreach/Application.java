package com.foreach;


import com.foreach.service.LogAnalyzerService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements ApplicationRunner {
    private static final String ARG_FILE_PATH = "filePath";
    private LogAnalyzerService logAnalyzerService;

    public Application(LogAnalyzerService logAnalyzerService) {
        this.logAnalyzerService = logAnalyzerService;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(ApplicationArguments args) {
        String filePath = null;
        if (args.containsOption(ARG_FILE_PATH)) {
            filePath = args.getOptionValues(ARG_FILE_PATH).stream().findFirst().orElse(null);
        }
        logAnalyzerService.analyzeLog(filePath);
    }
}

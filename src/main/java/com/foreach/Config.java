package com.foreach;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
    @Value("${log.file-path}")
    private String logFilePath;
    @Value("${log.duration-limit}")
    private Long durationLimit;
    @Value("${queue.capacity}")
    private Integer queueCapacity;
    @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
    private Integer dbBatchSize;

    public String getLogFilePath() {
        return logFilePath;
    }

    public Long getDurationLimit() {
        return durationLimit;
    }

    public Integer getQueueCapacity() {
        return queueCapacity;
    }

    public Integer getDbBatchSize() {
        return dbBatchSize;
    }
}

package com.foreach.model;


import javax.validation.constraints.NotNull;
import java.util.Objects;

public class EventData {
    private String id;
    private State state;
    private long timestamp;
    private String type;
    private String host;

    public EventData() {
    }

    public EventData(String id, State state, long timestamp, String type, String host) {
        this.id = id;
        this.state = state;
        this.timestamp = timestamp;
        this.type = type;
        this.host = host;
    }

    public static EventDataBuilder builder() {
        return new EventDataBuilder();
    }

    public EventDataBuilder toBuilder() {
        return builder()
                .withHost(host)
                .withId(id)
                .withState(state)
                .withTimestamp(timestamp)
                .withType(type);
    }

    @NotNull(message = "Id cannot be null")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @NotNull
    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @NotNull
    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public enum State {
        STARTED,
        FINISHED
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EventData eventData = (EventData) o;
        return timestamp == eventData.timestamp &&
                id.equals(eventData.id) &&
                state == eventData.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, state, timestamp);
    }

    public static final class EventDataBuilder {
        private String id;
        private State state;
        private long timestamp;
        private String type;
        private String host;

        private EventDataBuilder() {
        }

        public EventDataBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public EventDataBuilder withState(State state) {
            this.state = state;
            return this;
        }

        public EventDataBuilder withTimestamp(long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public EventDataBuilder withType(String type) {
            this.type = type;
            return this;
        }

        public EventDataBuilder withHost(String host) {
            this.host = host;
            return this;
        }

        public EventData build() {
            return new EventData(id, state, timestamp, type, host);
        }
    }
}




Log analyzer
===============================

The purpose of this project is to efficiently analyze huge files of logs in specific format and persist it in hsdb database.

Logs example
-----------------------------------------------------------
    {"id":"scsmbstgra", "state":"STARTED", "type":"APPLICATION_LOG", "host":"12345", "timestamp":1491377495212}
    {"id":"scsmbstgrb", "state":"STARTED", "timestamp":1491377495213}
    {"id":"scsmbstgrc", "state":"FINISHED", "timestamp":1491377495218}
    {"id":"scsmbstgra", "state":"FINISHED", "type":"APPLICATION_LOG", "host":"12345", "timestamp":1491377495217}
    {"id":"scsmbstgrc", "state":"STARTED", "timestamp":1491377495210}
    {"id":"scsmbstgrb", "state":"FINISHED", "timestamp":1491377495216}

Requirements
------------
- Java 1.8
- Maven 3+ or Gradle

How to use ?
---------------
1. Prepare a log file with follows above example. You have three options to do that:
    1) update file located in resources/log-example.txt
    2) create a new file and update path in resources/application.properties
    3) create a new file and run program with param: --filePath={absolutePathToFile} where you need to update "{absolutePathToFile}" 
    with absolute path to your newly created file
2. Run program with (maven): 
`mvn spring-boot:run`
or to run with specific path to log:
`mvn spring-boot:run -Dspring-boot.run.arguments=--filePath={absolutePathToFile}`
3. Or run with gradle:
 `gradle bootRun `
 or to run with specific path to log:
 `gradle bootRun -Pargs=--filePath={absolutePathToFile}`
